# Publiccode.Yml

En listning av alla tillkännagivna öppna programvaror som används eller publicerats av offentlig sektor. Syftet är att beskriva dessa programvaror enligt en standard för att få en robust datakälla.

## Plan

Översätta alla programvaror här:
https://gitlab.com/open-data-knowledge-sharing/katalogen/-/blob/community/content/katalog/programvaror.md

Enligt standarden:
https://github.com/publiccodeyml/publiccode.yml

Med konventionen:
Katalognamn: Ett representativt namn på programvaran
Katalognamn/publiccode.yml: Fil innehållande en beskrivning av programvaran

1. Målet är att i Mars/April ha en datakälla.
2. Steg två är att ha ett öppet API för att nå denna information
3. Utreda om vi kan "skrapa" GitHub/Gitlab efter repon som innehåller publiccode.yml
4. Bygga ett enklare UI som publiceras på offentligkod.se
